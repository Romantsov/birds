var videoPath = 'video.mp4';
const fill = document.querySelectorAll('.fill');
const empties = document.querySelectorAll('.empty');
const dolls = document.querySelectorAll('.bird-doll');
const tools = document.querySelectorAll('.bird-song');
var divDrawing = document.querySelector('.drawing-print');
var divPrint = document.querySelector('#print');
var drawing = document.querySelector('#drawingImg');
var reload = document.querySelector('.reload-game');
var videoPlayer = document.querySelector('#video-player')
var cBg; // Current background image control

var soundBird1 = new Audio('assets/snd/duck_woodcall.wav');
var soundBird2 = new Audio('assets/snd/skylark_woodcall.wav');
var soundBird3 = new Audio('assets/snd/warbler_woodcall.mp3');
var soundBird4 = new Audio('assets/snd/nightingale_woodcall.mp3');
var soundBird5 = new Audio('assets/snd/cuckoo_woodcall.mp3');
var soundBird6 = new Audio('assets/snd/owl_woodcall.mp3');

var songBird1 = new Audio('assets/snd/Duck.wav');
var songBird2 = new Audio('assets/snd/skylark.mp3');
var songBird3 = new Audio('assets/snd/warbler.mp3');
var songBird4 = new Audio('assets/snd/nightingale.wav');
var songBird5 = new Audio('assets/snd/cuckoo.wav');
var songBird6 = new Audio('assets/snd/owl.mp3');

var countBlock1 = false;
var countBlock2 = false;
var countBlock3 = false;
var countBlock4 = false;
var countBlock5 = false;
var countBlock6 = false;

var counts = [countBlock1, countBlock2, countBlock3, countBlock4, countBlock5, countBlock6];
var birdSongs = [songBird1, songBird2, songBird3, songBird4, songBird5, songBird6];

var counterCorrect = 0;
var closeCount = 0;

var selectedElement;

var request;

var matchBird1 = {
    name: "duck",
    img: "url(assets/img/duck.png)",
};

var matchBird2 = {
    name: "skylark",
    img: "url('assets/img/skylark.png')",
};

var matchBird3 = {
    name: "warbler",
    img: "url('assets/img/warbler.png')",
};

var matchBird4 = {
    name: "nightingale",
    img: "url('assets/img/nightingale.png')",
};

var matchBird5 = {
    name: "cuckoo",
    img: "url('assets/img/cuckoo.png')",
};

var matchBird6 = {
    name: "owl",
    img: "url('assets/img/little_owl.png')",
};

var matches = [matchBird1, matchBird2, matchBird3, matchBird4, matchBird5, matchBird6];

// Hover listeners for sound
// First bird
dolls[0].addEventListener('mouseover', function(){
    songBird1.play();
    // songBird1.volume(0.1);
});
dolls[0].addEventListener('mouseout', function(){
    songBird1.pause();
    songBird1.currentTime = 0;
});

// Second bird
dolls[1].addEventListener('mouseover', function(){
    songBird2.play();
});
dolls[1].addEventListener('mouseout', function(){
    songBird2.pause();
    songBird2.currentTime = 0;
});

// Third bird
dolls[2].addEventListener('mouseover', function(){
    songBird3.play();
});
dolls[2].addEventListener('mouseout', function(){
    songBird3.pause();
    songBird3.currentTime = 0;
});

// Fourth bird
dolls[3].addEventListener('mouseover', function(){
    songBird4.play();
});
dolls[3].addEventListener('mouseout', function(){
    songBird4.pause();
    songBird4.currentTime = 0;
});

// Fifth bird
dolls[4].addEventListener('mouseover', function(){
    songBird5.play();
});
dolls[4].addEventListener('mouseout', function(){
    songBird5.pause();
    songBird5.currentTime = 0;
});

// Sixth bird
dolls[5].addEventListener('mouseover', function(){
    songBird6.play();
});
dolls[5].addEventListener('mouseout', function(){
    songBird6.pause();
    songBird6.currentTime = 0;
});

// Instruments to match
// First instrument
tools[0].addEventListener('mouseover', function(){
    soundBird1.play();
});
tools[0].addEventListener('mouseout', function(){
    soundBird1.pause();
    soundBird1.currentTime = 0;
});
 
// Second instrument
tools[1].addEventListener('mouseover', function(){
    soundBird2.play();
});
tools[1].addEventListener('mouseout', function(){
    soundBird2.pause();
    soundBird2.currentTime = 0;
});
 
// Third instrument
tools[2].addEventListener('mouseover', function(){
    soundBird3.play();
})
tools[2].addEventListener('mouseout', function(){
    soundBird3.pause();
    soundBird3.currentTime = 0;
});
 
// Fourth instrument
tools[3].addEventListener('mouseover', function(){
    soundBird4.play();
});
tools[3].addEventListener('mouseout', function(){
    soundBird4.pause();
    soundBird4.currentTime = 0;
});
 
// Fifth instrument
tools[4].addEventListener('mouseover', function(){
    soundBird5.play();
})
tools[4].addEventListener('mouseout', function(){
    soundBird5.pause();
    soundBird5.currentTime = 0;
});
 
// Sixth instrument
tools[5].addEventListener('mouseover', function(){
    soundBird6.play();
});
tools[5].addEventListener('mouseout', function(){
    soundBird6.pause();
    soundBird6.currentTime = 0;
});
 
// Drag and drop test
// Fill listeners
for (const filler of fill){
    filler.addEventListener('dragstart', dragStart); // causes errors?
    filler.addEventListener('dragend', dragEnd); // causes errors?
}


// Loop through empties and call drag events
for(const empty of empties){
    empty.addEventListener('dragover', dragOver);
    empty.addEventListener('dragenter', dragEnter);
    empty.addEventListener('dragleave', dragLeave);
    empty.addEventListener('drop', dragDrop);
}

// Drag Functions
function dragStart(e){
    // the first line duplicates the image, the 2nd line makes this image disappear.
    // without setTimeout() the image disappears instantly after picking it up.
    selectedElement = e.target.id;
    // this.className += ' hold';
    setTimeout(() => (this.className = 'invisible'), 0); // Doesn't turn the image invisible
};

function dragEnd(){
    this.className = 'fill';
};

function dragOver(e){
    e.preventDefault(); // Prevents default html/css behaviour
    
};

function dragEnter(e){
    e.preventDefault();
    if (selectedElement === 'img-duck') {
        cBg = this.style.backgroundImage
        if (countBlock1 === false) {
            this.style.backgroundImage = matchBird1.img;
            this.style.backgroundSize = 'contain';
            this.style.backgroundPosition = 'center center';
            this.style.backgroundRepeat = 'no-repeat';
        } else {this.style.backgroundImage = cBg}
    }
    else if (selectedElement === 'img-owl') {
        cBg = this.style.backgroundImage
        if (countBlock2 === false) {
            this.style.backgroundImage = matchBird6.img;
            this.style.backgroundSize = 'contain';
            this.style.backgroundPosition = 'center center';
            this.style.backgroundRepeat = 'no-repeat';
        } else {this.style.backgroundImage = cBg}
    }
    else if (selectedElement === 'img-skylark') {
        cBg = this.style.backgroundImage
        if (countBlock3 === false) {
            this.style.backgroundImage = matchBird2.img;
            this.style.backgroundSize = 'contain';
            this.style.backgroundPosition = 'center center';
            this.style.backgroundRepeat = 'no-repeat';
        } else {this.style.backgroundImage = cBg}
    }
    else if (selectedElement === 'img-warbler') {
        cBg = this.style.backgroundImage
        if (countBlock4 === false) {
            this.style.backgroundImage = matchBird3.img;
            this.style.backgroundSize = 'contain';
            this.style.backgroundPosition = 'center center';
            this.style.backgroundRepeat = 'no-repeat';
        } else {this.style.backgroundImage = cBg}
    }
    else if (selectedElement === 'img-nightingale') {
        cBg = this.style.backgroundImage
        if (countBlock5 === false) {
            this.style.backgroundImage = matchBird4.img;
            this.style.backgroundSize = 'contain';
            this.style.backgroundPosition = 'center center';
            this.style.backgroundRepeat = 'no-repeat';    
        } else {this.style.backgroundImage = cBg}
    }
    else if (selectedElement === 'img-cuckoo') {
        cBg = this.style.backgroundImage
        if (countBlock6 === false) {
            this.style.backgroundImage = matchBird5.img;
            this.style.backgroundSize = 'contain';
            this.style.backgroundPosition = 'center center';
            this.style.backgroundRepeat = 'no-repeat';
        } else {this.style.backgroundImage = cBg}
    }
}

function dragLeave(){
    if (countBlock1 === false){
        this.style.backgroundImage = 'none';
    } else {this.style.backgroundImage = cBg}
    if (countBlock2 === false){
        this.style.backgroundImage = 'none';
    } else {this.style.backgroundImage = cBg}
    if (countBlock3 === false){
        this.style.backgroundImage = 'none';
    } else {this.style.backgroundImage = cBg}
    if (countBlock4 === false){
        this.style.backgroundImage = 'none';
    } else {this.style.backgroundImage = cBg}
    if (countBlock5 === false){
        this.style.backgroundImage = 'none';
    } else {this.style.backgroundImage = cBg}
    if (countBlock6 === false){
        this.style.backgroundImage = 'none';
    } else {this.style.backgroundImage = cBg}
}

function dragDrop(){
    if (this.getAttribute('id') === 'duck-mallard' && selectedElement === 'img-duck'){
        if (countBlock1 == false){
            counterCorrect += 1;
            drawingName = 'duck';
            displayDrawing(drawingName);
            countBlock1 = true;
            document.getElementById(selectedElement).style.display = 'none';
        } else {this.style.backgroundImage = cBg}
    }
    if (this.getAttribute('id') === 'little-owl' && selectedElement === 'img-owl'){
        
        if (countBlock2 == false){
            counterCorrect += 1;
            drawingName = 'owl';
            displayDrawing(drawingName);
            countBlock2 = true;
            document.getElementById(selectedElement).style.display = 'none';
        } else {this.style.backgroundImage = cBg}
    }
    if (this.getAttribute('id') === 'skylark' && selectedElement === 'img-skylark'){
        
        if (countBlock3 == false){
            counterCorrect += 1;
            drawingName = 'skylark';
            displayDrawing(drawingName);
            countBlock3 = true;
            document.getElementById(selectedElement).style.display = 'none';
        } else {this.style.backgroundImage = cBg}
    }
    if (this.getAttribute('id') === 'garden-warbler' && selectedElement === 'img-warbler'){
        
        if (countBlock4 == false){
            counterCorrect += 1;
            drawingName = 'warbler';
            displayDrawing(drawingName);
            countBlock4 = true;
            document.getElementById(selectedElement).style.display = 'none';
        } else {this.style.backgroundImage = cBg}
    }
    if (this.getAttribute('id') === 'nightingale' && selectedElement === 'img-nightingale'){
        
        if (countBlock5 == false){
            counterCorrect += 1;
            drawingName = 'nightingale';
            displayDrawing(drawingName);
            countBlock5 = true;
            document.getElementById(selectedElement).style.display = 'none';
        } else {this.style.backgroundImage = cBg}
    } 
    if (this.getAttribute('id') === 'cuckoo' && selectedElement === 'img-cuckoo'){
        
        if (countBlock6 === false){
            counterCorrect += 1;
            drawingName = 'cuckoo';
            displayDrawing(drawingName);
            countBlock6 = true;
            document.getElementById(selectedElement).style.display = 'none';
        } else {this.style.backgroundImage = cBg}
    } 
}

var divTest = document.querySelector('.drawing-print');
var prtTest = document.querySelector('#print');
var closeWindow = document.querySelector('#close');
var divShow = false;
var drawingName;

closeWindow.addEventListener('click', hideDrawing);

// Drawing url's
dLink1 = 'assets/img/drawing_duck.jpg'
dLink2 = 'assets/img/drawing_owl.jpg'
dLink3 = 'assets/img/drawing_skylark.jpg'
dLink4 = 'assets/img/drawing_warbler.jpg'
dLink5 = 'assets/img/drawing_nightingale.jpg'
dLink6 = 'assets/img/drawing_cuckoo.jpg'

function displayDrawing(drawingName){
    if (divShow === false) {
        divTest.style.display = 'block';
            if (closeCount <= 6){
            prtTest.style.display = 'block';
        } else if (closeCount === 6 ) {prtTest.style.display = 'none'};
        drawing.style.display = 'block';
        divShow = true;
        if (drawingName === 'duck'){
            drawing.src = dLink1;
        }
        else if (drawingName === 'owl'){
            drawing.src = dLink2;
        }
        else if (drawingName === 'skylark'){
            drawing.src = dLink3;
        }
        else if (drawingName === 'warbler'){
            drawing.src = dLink4;
        }
        else if (drawingName === 'nightingale'){
            drawing.src = dLink5;
        }
        else if (drawingName === 'cuckoo'){
            drawing.src = dLink6;
        }
        else if (drawingName === 'video'){
            videoPlayer.src = "assets/vid/" + videoPath;
            prtTest.style.display = 'none';
            drawing.style.display = 'none';
        }
    }
};

function hideDrawing(){
    divTest.style.display = 'none';
        prtTest.style.display = 'none';
        divShow = false;
        closeCount += 1;
        drawing.style.display = 'none';
        if (closeCount === 6) {
            drawingName = 'video';
            displayDrawing(drawingName);
            videoPlayer.style.display = 'block';
            divDrawing.style.backgroundColor = 'black';
        }
    };

// Print function
// var source;

// function imageToPrint() {
//     return "<html><head><script>function step1(){\n" +
//             "setTimeout('step2()', 10);}\n" + 
//             "function step2(){window.print();window.close()}\n" +
//             "</scri" + "pt></head><body onload='step1()'>\n" +
//             "<img src='" + source + "' /><body></html>";
// }
    
divPrint.addEventListener('click', printDrawing)
function printDrawing(){
    if (drawingName === 'duck') {
        setTimeout("printJS(dLink1, 'image')", 0)
    }
    if (drawingName === 'owl') {
        setTimeout("printJS(dLink2, 'image')", 0)
    }
    if (drawingName === 'skylark') {    
        setTimeout("printJS(dLink3, 'image')", 0)
    }
    if (drawingName === 'warbler') {      
        setTimeout("printJS(dLink4, 'image')", 0)
    }
    if (drawingName === 'nightingale') {        
        setTimeout("printJS(dLink5, 'image')", 0)
    }
    if (drawingName === 'cuckoo') {
        setTimeout("printJS(dLink6, 'image')", 0)
    }
};

// Reload game function
reload.addEventListener('click', function() {
    for(var doll of dolls){
        doll.className = 'bird-doll empty';
        doll.style.backgroundImage = 'none';
    }
        divShow = false;
        divTest.style.display = 'none';
        prtTest.style.display = 'none';
        counterCorrect = 0;
        closeCount = 0;
        countBlock1 = false;
        countBlock2 = false;
        countBlock3 = false;
        countBlock4 = false;
        countBlock5 = false;
        countBlock6 = false;
        fill[0].style.display = 'block';
        fill[1].style.display = 'block';
        fill[2].style.display = 'block';
        fill[3].style.display = 'block';
        fill[4].style.display = 'block';
        fill[5].style.display = 'block';
        drawingName = '';
        selectedElement = '';
        videoPlayer.style.display = 'none';
        divDrawing.style.backgroundColor = '#dddddd';
});